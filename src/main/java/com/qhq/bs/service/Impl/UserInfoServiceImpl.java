package com.qhq.bs.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qhq.bs.mapper.UsersMapper;
import com.qhq.bs.entity.Users;
import com.qhq.bs.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserInfoServiceImpl extends ServiceImpl<UsersMapper, Users> implements UserInfoService{

    @Autowired
    private UsersMapper usersMapper;

    @Override
    public void add(Users users) {
        this.saveOrUpdate(users);
    }

    @Override
    public void delete(Integer id) {
        usersMapper.delete(id);
    }

    @Override
    public void update(Users users) {
        usersMapper.updateById(users);
    }

    @Override
    public Users queryById(Integer id) {
        return usersMapper.queryById(id);
    }

    @Override
    public List<Users> queryAll() {
        return usersMapper.queryAll();
    }
}