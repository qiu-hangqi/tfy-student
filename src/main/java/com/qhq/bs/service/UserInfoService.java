package com.qhq.bs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qhq.bs.entity.Users;

import java.util.List;
public interface UserInfoService extends IService<Users> {
    /**
     * 增加一条数据
     * @param users 数据
     */
    void add(Users users);

    /**
     * 删除一条数据
     * @param id 被删除数据的id
     */
    void delete(Integer id);

    /**
     * 修改一条数据
     * @param users 修改的数据
     */
    void update(Users users);

    /**
     * 根据id去查询一条数据
     * @param id 查询的id
     */
    Users queryById(Integer id);

    /**
     * 查询全部数据
     * @return
     */
    List<Users> queryAll();
}
