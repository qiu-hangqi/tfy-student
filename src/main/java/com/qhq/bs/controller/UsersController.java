package com.qhq.bs.controller;

import com.qhq.bs.entity.Users;
import com.qhq.bs.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/user")
public class UsersController {
    @Autowired
    private UserInfoService userInfoService;

    @RequestMapping("/list")
    public List<Users> queryAll(){
        return userInfoService.queryAll();
    }

    @RequestMapping("/getOne")
    public List<Users> query(Integer id){
        Users users = userInfoService.queryById(id);
        List<Users> usersList = new ArrayList<>();
        usersList.add(users);
        return usersList;
    }

    @RequestMapping("/addUser")
    public String add(@RequestBody Users users){
        userInfoService.add(users);
        return "添加OK";
    }

    @RequestMapping("/delete")
    public String delete(Integer id){
        userInfoService.delete(id);
        return "删除成功";
    }

    @RequestMapping("/update")
    public String update(@RequestBody Users users){
        userInfoService.update(users);
        return "修改成功";
    }
}