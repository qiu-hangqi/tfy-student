package com.qhq.bs.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.qhq.bs.common.utils.StringUtils;
import com.qhq.bs.entity.Users;
import com.qhq.bs.response.AjaxResult;
import com.qhq.bs.service.UserInfoService;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
@RestController
@RequestMapping("/login")
public class LoginController {

    @Resource
    private UserInfoService userInfoService;

    @RequestMapping("/check")
    public AjaxResult ckeckUserInfo(String phone,String password){
        try {
            if(StringUtils.isEmpty(phone) || StringUtils.isEmpty(password)){
                throw new RuntimeException("请完善登录信息");
            }
            Users users = userInfoService.getOne(
                    new LambdaQueryWrapper<Users>()
                            .eq(Users::getPhone,phone)
                            .eq(Users::getPassword,password)
                            .last("limit 1")
            );
            if(ObjectUtils.isEmpty(users)){
                throw new RuntimeException("用户名密码错误");
            }
            AjaxResult ajaxResult = AjaxResult.success();
            ajaxResult.put("userInfo", users);
            ajaxResult.put("userType", users.getUserType());
            return ajaxResult;
        } catch (RuntimeException e){
            return AjaxResult.error(e.getMessage());
        }
    }

}
