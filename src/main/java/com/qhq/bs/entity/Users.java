package com.qhq.bs.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import javax.persistence.Table;

/**
 * 用户信息表
 */
@Data
@Table(name = "users")
public class Users {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String name;
    private String phone;
    private String password;
    private String userType;
}
