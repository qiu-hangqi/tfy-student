package com.qhq.bs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qhq.bs.entity.Users;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsersMapper extends BaseMapper<Users> {
    /**
     * 删除一条数据
     * @param id 被删除数据的id
     */
    void delete(Integer id);

    /**
     * 修改一条数据
     * @param users 修改的数据
     */
    void update(Users users);

    /**
     * 根据id去查询一条数据
     * @param id 查询的id
     */
    Users queryById(Integer id);

    /**
     * 查询全部数据
     * @return
     */
    List<Users> queryAll();

}
